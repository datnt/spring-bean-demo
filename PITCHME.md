# Spring boot dependency injection
- Configuration instructions
- Bean life cycle

---

![Spring boot DI](assets/di.png)

---

# Configuration instructions
- java-based configuraiton
- annotation-based configuration
- xml-based configuration

---

## java-based configuration
```java
@Configuration
public class SwaggerConfiguration {
    @Bean
    public Docket newsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .paths(regex("/help/chat/.*"))
                .build();
    }


    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Spring REST Sample with Swagger")
                .description("Lazada User REST Service with Swagger")
                .version("2.0")
                .build();
    }
}
```
---
## annotation-based configuration
- `@ComponentScan`

```java
@Service
public class ChatbotSwitcher {
    
}
```

```java
public class ChatbotController {

    @Autowired
    private ChatbotSwitcher chatbotSwitcher;
}
```
---
![annotation configuration](assets/annotation.png)

---
## xml-based configuration
```xml
<?xml version="1.0" encoding="GBK"?>
<!DOCTYPE beans PUBLIC "-//SPRING//DTD BEAN//EN" "classpath:dtd/spring-beans.dtd">
<beans default-autowire="byName">

	<import resource="classpath:sentinel-tracer-entrance.xml"/>
	<import resource="classpath:sentinel-tracer-proxy.xml"/>


	
	<!--  sentinel启动时资源初始化bean -->
	<bean id="sentinelInitBean" 
    	class="com.taobao.csp.sentinel.util.SentinelInitBean" init-method="init">
  	</bean>

</beans>
```
---
# Bean life-cycle
![Bean life-cycle](assets/bean-life-cycle.png)

---
## Initialization bean
![Initialization bean](assets/design-pattern.png)

---
## scope of bean
- singleton scope
- prototype scope
- session scope
- request scope

---
### Singleton scope
![Singleton scope](https://docs.spring.io/spring/docs/3.0.0.M3/reference/html/images/singleton.png)

---
### Prototype scope
![Prototype scope](https://docs.spring.io/spring/docs/3.0.0.M3/reference/html/images/prototype.png)

---
## use phrase
- no proxy
- jdk proxy
- cglib proxy

---
### no proxy
![no proxy](http://blog.springsource.org/wp-content/uploads/2012/05/no-proxy.png)

---
### jdk proxy
![jkd proxy](https://www.credera.com/wp-content/uploads/2016/04/Picture2-4.png)

---
### cglib proxy
![cglib proxy](https://www.credera.com/wp-content/uploads/2016/04/xPicture3-3.png.pagespeed.ic.W2zdwkeosT.webp)
---
# Reference
- Spring 5 design pattern (https://www.amazon.com/Spring-Design-Patterns-application-development-ebook/dp/B075F67NWP)
- Transactions, Caching and AOP: understanding proxy usage in Spring (https://spring.io/blog/2012/05/23/transactions-caching-and-aop-understanding-proxy-usage-in-spring)
- Aspect-Oriented Programming in Spring Boot Part 2: Spring JDK Proxies vs CGLIB vs AspectJ (https://www.credera.com/blog/technology-insights/open-source-technology-insights/aspect-oriented-programming-in-spring-boot-part-2-spring-jdk-proxies-vs-cglib-vs-aspectj/)