package pro.datnt.spring.bean.demo.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.Instant;

@Component
@Slf4j
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RequestScopeBean {

    private Instant instant;

    public RequestScopeBean() {
        instant = Instant.now();
        log.info("call RequestScopeBean(), instant {}", instant.toString());
    }

    public String getInstant() {
        return instant.toString();
    }

    @PreDestroy
    public void preDestroy() {
        log.info("call preDestroy(), instant {}", instant.toString());
    }

    @PostConstruct
    public void postConstruct() {
        log.info("postConstruct was called, instant {}", instant.toString());
    }
}
