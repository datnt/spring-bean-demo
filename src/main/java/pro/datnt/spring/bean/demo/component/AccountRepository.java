package pro.datnt.spring.bean.demo.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;

@Repository(value="thisIsBeanName")
@Slf4j
public class AccountRepository {
    @PostConstruct
    public void populateCache() {
        log.info("populateCache as call");
    }
}
