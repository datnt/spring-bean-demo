package pro.datnt.spring.bean.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pro.datnt.spring.bean.demo.component.PrototypeScopeBean;
import pro.datnt.spring.bean.demo.component.RequestScopeBean;
import pro.datnt.spring.bean.demo.component.SingletonScopeBean;

import javax.annotation.Resource;

@RestController
public class IndexController {
    @Autowired
    private PrototypeScopeBean prototypeScopeBean;

    @Autowired
    private SingletonScopeBean singletonScopeBean;

    @Resource(name = "requestScopeBean")
    private RequestScopeBean requestScopeBean;

    @RequestMapping("/")
    public String index() {
        singletonScopeBean.getString();
        return "test " + requestScopeBean.getInstant();
    }
}
