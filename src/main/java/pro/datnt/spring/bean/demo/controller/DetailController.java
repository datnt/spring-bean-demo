package pro.datnt.spring.bean.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pro.datnt.spring.bean.demo.component.PrototypeScopeBean;
import pro.datnt.spring.bean.demo.component.SessionScopeBean;

import javax.annotation.Resource;

@RestController
public class DetailController {
    @Autowired
    private PrototypeScopeBean prototypeScopeBean;

    @Resource(name="sessionScopeBean")
    private SessionScopeBean sessionScopeBean;

    @RequestMapping("/detail")
    public String index() {
        return "detail " + sessionScopeBean.getInstant();
    }
}
