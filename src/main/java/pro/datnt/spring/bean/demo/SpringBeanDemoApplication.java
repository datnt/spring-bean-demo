package pro.datnt.spring.bean.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
//@EnableCaching
public class SpringBeanDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBeanDemoApplication.class, args);
    }
}
