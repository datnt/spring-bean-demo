package pro.datnt.spring.bean.demo.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
@Slf4j
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrototypeScopeBean {
    @PreDestroy
    public void preDestroy() {
        log.info("call preDestroy()");
    }

    @PostConstruct
    public void postConstruct() {
        log.info("postConstruct was called");
    }
}

