package pro.datnt.spring.bean.demo.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
@Slf4j
public class SingletonScopeBean {
    @PreDestroy
    public void preDestroy() {
        log.info("call preDestroy()");
    }

    //@Cacheable(value = "getString")
    public String getString() {
        return "String " + this.getClass().getName();
    }
}
